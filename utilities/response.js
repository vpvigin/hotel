const success = async (res, status, code, message = null, data = null) => {
    let response = {
        status: status,
        message: message,
        data: data
    };

    res.status(code);
    res.json(response);
};

const failure = async (res, code, message = null, errors = null) => {
    let response = {
        status: false,
        message: message,
        errors: errors
    };

    res.status(code);
    res.json(response);
};

module.exports = {
    success,
    failure
};
