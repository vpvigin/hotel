const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
});
mongoose.Promise = global.Promise;

const debugMode = process.env.ENV === 'DEV' ? 'debug' : '';
mongoose.set(debugMode, (collectionName, method, query, doc) => {
    console.log(`${collectionName}.${method}`, JSON.stringify(query), doc);
});

mongoose.connection.on('connected', () => {
    console.log(`Mongoose default connection is open to ${process.env.MONGODB_URI}`);
});

mongoose.connection.on('error', (err) => {
    console.log(`Mongoose default connection has occured ${err} error`);
});

mongoose.connection.on('disconnected', () => {
    console.log(`Mongoose default connection is disconnected`);
});

process.on('SIGINT', () => {
    mongoose.connection.close(() => {
        console.log(`Mongoose default connection is disconnected due to application termination`);
        process.exit(0);
    });
});

module.exports = {
    User: require('../models/test')

};
