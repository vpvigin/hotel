const express = require('express');
const router = express.Router();

const {
  getUserTypes
} = require('../modules/users/userController');


router.get('/getUserTypes', getUserTypes);

module.exports = router;
