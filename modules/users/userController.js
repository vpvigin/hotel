const response = require('../../utilities/response');
const constants = require('../../config/constants.json');

const getUserTypes = async (req, res, next) => {
    try {
        let message = null;
        let data = constants.USERS.ROLE
        response.success(res, true, 200, message, data);
        return;
    } catch (error) {
        let message = 'Error in fetching'
        response.failure(res, false, 400, message, data);
        return;
    }
};

module.exports = {
    getUserTypes
};
