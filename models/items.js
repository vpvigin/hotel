const mongoose = require('mongoose');
const Double = require('@mongoosejs/double');
const Schema = mongoose.Schema;

const schema = new Schema({
    food_name: String,
    food_type:Number,
    price: Double
},
{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }});
module.exports = mongoose.model('items', schema);