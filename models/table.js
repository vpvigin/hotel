const mongoose = require('mongoose');
const Double = require('@mongoosejs/double');
const Schema = mongoose.Schema;

const schema = new Schema({
    table_id: Number,
    table_seats: Array,
    item:
        {
            item_id: { type: Schema.Types.ObjectId, ref: 'items' },
            item_price: Double
        }
},
{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }});
module.exports = mongoose.model('tables', schema);